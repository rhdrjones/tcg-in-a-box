
if [ -n "${QEMU}" ]; then
	if ! test -x "${QEMU}"; then
		echo "${QEMU} not found or is not executable." >&2
		exit 1
	fi
elif test -x /usr/bin/qemu-system-aarch64; then
	QEMU=/usr/bin/qemu-system-aarch64
elif test -x /usr/libexec/qemu-kvm; then
	QEMU=/usr/libexec/qemu-kvm
else
	echo "No qemu binary found." >&2
	exit 1
fi

QVER=$(${QEMU} -h | head -1 | cut -d' ' -f4)
QVER_MAJOR=$(echo "${QVER}" | cut -d. -f1)
QVER_MINOR=$(echo "${QVER}" | cut -d. -f2)

if [ "${QVER_MAJOR}" -lt 6 ] || [ "${QVER_MINOR}" -lt 1 ]; then
	echo "QEMU version 6.1 or later required"
	exit 1
fi

EDK2=/usr/share/edk2/aarch64/QEMU_EFI-silent-pflash.raw
if ! test -e "${EDK2}"; then
	EDK2=/usr/share/edk2/aarch64/QEMU_EFI-pflash.raw
	if ! test -e "${EDK2}"; then
		echo "Can't find UEFI for the VM."
		echo "Try dnf install edk2-aarch64"
		exit 1
	fi
fi

QCOW2=cs9-qemu-qa-regular.aarch64.qcow2
if [ -n "$1" ]; then
	QCOW2=$1
fi
if ! test -f "${QCOW2}"; then
	echo "${QCOW2} not found." >&2
	exit 1
fi

if [ "$(arch)" = "aarch64" ] && [ -w /dev/kvm ]; then
	ACCEL=kvm
	GICV=host
	CPU=host
else
	ACCEL=tcg
	GICV=2
	CPU=cortex-a57
fi

SMP=$(nproc)
if [ "${SMP}" -gt 8 ]; then
	SMP=8
fi

SYSMEM=$(awk '/MemTotal:/ {print$2}' /proc/meminfo)
MEM=4096
SYSMEM=$((SYSMEM / 1024))
m=$((SYSMEM - MEM))
while [ "${m}" -lt 1024 ]; do
	MEM=$((MEM - 1024))
	m=$((SYSMEM - MEM))
done
if [ "${MEM}" -le 0 ]; then
	echo "Insufficient memory available to run VM"
	exit 1
fi

if [ -n "${MAC}" ]; then
	m=$(echo "${MAC}" | awk '/^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$/ {print$1}')
	if [ "${m}" != "${MAC}" ]; then
		echo "Bad MAC address ${MAC}." >&2
		exit 1
	fi
else
	r=$(uname -n | md5sum) # string of random hex, but reproducible on this host
	o1=$(echo "${r}" | cut -c1,2)
	o2=$(echo "${r}" | cut -c3,4)
	o3=$(echo "${r}" | cut -c5,6)
	MAC="52:54:00:${o1}:${o2}:${o3}" # first three octets are libvirt's first three
fi

if [ -n "${VIRTIOFSD}" ]; then
	if ! test -x "${VIRTIOFSD}"; then
		echo "${VIRTIOFSD} not found or is not executable." >&2
		exit 1
	fi
	if [ "$(id -u)" -ne 0 ]; then
		echo "Must execute ${VIRTIOFSD} as root." >&2
		exit 1
	fi
elif test -x /usr/libexec/virtiofsd; then
	VIRTIOFSD=/usr/libexec/virtiofsd
elif [ -n "${SHARED}" ]; then
	echo "virtiofsd not found. Cannot share ${SHARED}" >&2
	exit 1
fi

if [ -n "${VIRTIOFSD}" ] && [ "$(id -u)" -ne 0 ]; then
	VIRTIOFSD=
elif [ -z "${SHARED}" ] && [ ! -d /shared ]; then
	VIRTIOFSD=
fi

if [ -n "${VIRTIOFSD}" ]; then
	if [ -z "${SHARED}" ]; then
		SHARED=/shared
	fi
	if ! test -e "${SHARED}"; then
		mkdir "${SHARED}"
		chmod 777 "${SHARED}"
	elif ! test -d "${SHARED}"; then
		echo "${SHARED} is not a directory." >&2
		exit 1
	elif [ "$(stat -c '%a' "${SHARED}")" -ne 777 ]; then
		echo "${SHARED} must be readable and writable by all users (perm=777)." >&2
		exit 1
	fi

	m=$(df -k /dev/shm --output=avail | tail -1)
	m=$((m / 1024))
	if [ "${m}" -lt 768 ]; then
		echo "Insufficient shared memory (minimum size is 1G)"
		echo "Ensure --shm-size=<SIZE> is on the podman command line"
		exit 1
	fi

	MEM=${m}

	# Ensure we don't overcommit the host
	m=$((SYSMEM - MEM))
	if [ "${m}" -lt 1024 ]; then
		echo "Insufficient memory available to run VM"
		exit 1
	fi

	if "${VIRTIOFSD}" -h | grep -q 'source=PATH'; then
		# legacy virtiofsd cli
		"${VIRTIOFSD}" --socket-path=/tmp/vfsd.sock -o source="${SHARED}" -o cache=always &
	else
		"${VIRTIOFSD}" --socket-path=/tmp/vfsd.sock --shared-dir "${SHARED}" --sandbox none --modcaps=-mknod &
	fi
	sleep 3

	QEMU_CMDLINE_VIRTIOFS_EXT=" \
		-chardev socket,id=char0,path=/tmp/vfsd.sock \
		-device vhost-user-fs-pci,queue-size=1024,chardev=char0,tag=myfs \
		-object memory-backend-file,id=mem,size=${MEM}M,mem-path=/dev/shm,share=on \
		-numa node,memdev=mem"
fi

CMD="${QEMU} \
	-display none -no-user-config -nodefaults \
	-accel ${ACCEL} -machine virt,gic-version=${GICV} -cpu ${CPU} -m ${MEM} -smp ${SMP} \
	-bios ${EDK2} \
	-device pcie-root-port,port=0x8,chassis=1,id=pci.1,bus=pcie.0,multifunction=on,addr=0x1 \
	-device pcie-root-port,port=0x9,chassis=2,id=pci.2,bus=pcie.0,addr=0x1.0x1 \
	-device pcie-root-port,port=0xa,chassis=3,id=pci.3,bus=pcie.0,addr=0x1.0x2 \
	-device pcie-root-port,port=0xb,chassis=4,id=pci.4,bus=pcie.0,addr=0x1.0x3 \
	-netdev user,id=hostnet0,hostfwd=tcp::2222-:22 \
	-blockdev '{\"driver\":\"file\",\"filename\":\"${QCOW2}\",\"node-name\":\"disk-driver0\",\"discard\":\"unmap\"}' \
	-blockdev '{\"node-name\":\"disk-drive0\",\"read-only\":false,\"driver\":\"qcow2\",\"file\":\"disk-driver0\"}' \
	-object '{\"qom-type\":\"rng-random\",\"id\":\"objrng0\",\"filename\":\"/dev/urandom\"}' \
	-device virtio-net-pci,netdev=hostnet0,id=net0,mac=${MAC},bus=pci.1,addr=0x0 \
	-device virtio-scsi-pci,id=scsi0,bus=pci.2,addr=0x0 \
	-device virtio-blk-pci,bus=pci.3,addr=0x0,drive=disk-drive0,id=disk0,bootindex=1 \
	-device virtio-rng-pci,rng=objrng0,id=rng0,bus=pci.4,addr=0x0 \
	-serial mon:stdio"

if [ -n "${VIRTIOFSD}" ]; then
	CMD="${CMD} ${QEMU_CMDLINE_VIRTIOFS_EXT}"
fi

eval "${CMD}"
