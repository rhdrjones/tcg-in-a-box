# tcg-in-a-box

"TCG in a box" is simply a QEMU command line that launches a virtual machine, but QEMU and
the QEMU command line are bundled in a container in order to simplify the environment setup
and launching. The virtual machine (VM) runs the AArch64 CentOS Automotive SIG image. We call
this project *TCG* in a box rather than *VM* in a box because it's expected that the container
will mostly be run on x86 machines, requiring QEMU to use TCG in order to emulate an AArch64
CPU.

After launching the container, which may be launched detached, and waiting for the VM inside
it to boot, one may

- ssh and rsync directly to the VM using host-container port mapping
- share a specified host directory directly with the VM
- do rpm builds in the VM using the preinstalled rpm-build and dnf-plugins-core packages

Go to [Using the quay.io container](../main/quay.md) if you want to use the container from quay.io.

## Creating the container

### <a name="build-vm"></a>Build the VM (on an AArch64 machine)

Follow prep instructions at https://sigs.centos.org/automotive/building/

Build with
```
   $ cd automotive-sig/osbuild-manifests
   $ make cs9-qemu-qa-regular.aarch64.qcow2 DEFINES='image_size=17179869184 extra_rpms=["vim-minimal","rpm-build","dnf-plugins-core","rsync"]'
```

(This builds with a 16G disk. It uses the `qa` type because the qa type looks
 like the `minimal` type with sshd.)

### [optional] Extend the VM image (still on an AArch64 machine)

Ensure QEMU is installed, e.g.

```
   $ sudo dnf install qemu-system-aarch64-core
```

Then fetch the qemu.sh script and run it with the Automotive SIG qcow2 disk image,

```
   $ curl -O https://gitlab.com/rhdrjones/tcg-in-a-box/-/raw/main/qemu.sh
   $ bash qemu.sh path/to/cs9-qemu-qa-regular.aarch64.qcow2
```

(The path parameter is not needed if the image is in the current working directory.)

After the image boots, login with root (password is 'password') and do
whatever you like. For example, add the guest user to the sudoers file
to enable passwordless sudo. When complete, poweroff.

```
   # vi /etc/sudoers
   # poweroff
```

#### <a name="virtiofs-automount"></a>Set up hostfs share automounting

Another extension to the VM image that can be done by booting it on an AArch64
machine is the installation of a systemd service which will automount `/shared`
when a hostfs directory is shared with the VM using virtiofs. After booting
the VM, log in as root and do

```
   # curl -O https://gitlab.com/rhdrjones/tcg-in-a-box/-/raw/main/install-virtiofs-automount.sh
   # sh install-virtiofs-automount.sh
   # poweroff
```

### <a name="build-container"></a>Build the container (on an x86\_64 machine)

Prepare to build the container

```
   $ mkdir cvm
   $ cd cvm
```

scp over the image

```
   $ scp $AARCH64_HOST:path/to/cs9-qemu-qa-regular.aarch64.qcow2 .
```

Then,

```
   $ curl -O https://gitlab.com/rhdrjones/tcg-in-a-box/-/raw/main/Containerfile
   $ curl -O https://gitlab.com/rhdrjones/tcg-in-a-box/-/raw/main/qemu.sh
   $ cd ..
   $ podman build -t cvm-aarch64 cvm
```

### Create a bridge network for the container (on an x86\_64 machine)

```
   $ podman network create -d bridge cvm-bridge
```

(podman 3.0 or later is needed for rootless bridge networks)

## <a name="run-container"></a>Run the container without hostfs sharing

```
   $ podman run -d --network=cvm-bridge -p 2222:2222 localhost/cvm-aarch64
```

Wait a few minutes...

```
   $ ssh -p 2222 guest@localhost    # password is 'password'
```

## Run the container with hostfs sharing

```
   $ mkdir -p /path/to/shared
   $ chmod 777 /path/to/shared
   $ podman run -d --network=cvm-bridge -p 2222:2222 -v /path/to/shared:/shared:Z --shm-size=4G localhost/cvm-aarch64
```

Wait a few minutes...

```
   $ ssh -p 2222 guest@localhost    # password is 'password'
```

If the share hasn't already been automatically mounted (see [Set up hostfs share automounting](#virtiofs-automount)),
then mount the share with

```
   $ sudo mkdir -p /shared
   $ sudo mount -t virtiofs myfs /shared
```

## <a name="run-aarch64-container"></a>[optional] Run the container on an AArch64 machine

When running on an AArch64 machine we can accelerate the VM with KVM.

```
   $ podman run --security-opt label=disable --device=/dev/kvm $SAME_PARAMETERS_AS_WITHOUT_KVM
```

Wait 30 or 40 seconds...

```
   $ ssh -p 2222 guest@localhost    # password is 'password'
```

## Rebuild a source RPM in the AArch64 VM

After running the container and ssh'ing into the VM (see [Run the container ...](#run-the-container))

```
   $ sudo dnf --enablerepo=crb -y builddep $PACKAGE.src.rpm
   $ rpmbuild -ra $PACKAGE.src.rpm
```

## [optional] Simplify access

```
$ cat <<EOF >>.ssh/config

Host autosig
  Hostname localhost
  User guest
  Port 2222
EOF
$ ssh-copy-id autosig  # password is 'password'
$ ssh autosig
```

(The first login takes several seconds, but later logins are faster.)
