cat <<EOF >/sbin/virtiofs-shared-automount
#!/bin/sh
if grep -q virtiofs /proc/filesystems; then
    mkdir -p /shared
    mount -t virtiofs myfs /shared
fi
EOF
chmod +x /sbin/virtiofs-shared-automount

cat <<EOF >/etc/systemd/system/virtiofs-shared-automount.service
[Unit]
Description=VirtioFS Shared Automount

[Service]
ExecStart=/sbin/virtiofs-shared-automount

[Install]
WantedBy=multi-user.target
EOF

systemctl enable virtiofs-shared-automount
