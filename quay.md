# Using a TCG-in-a-box published container

## Pull the container
```
$ sudo dnf install podman
$ podman pull quay.io/drjones/cvm-aarch64
```

### Create a bridge network for the container

```
   $ podman network create -d bridge cvm-bridge
```

(podman 3.0 or later is needed for rootless bridge networks)

## Start the container
```
$ mkdir shared && chmod 777 shared
$ podman run -d --network=cvm-bridge -p 2222:2222 -v $PWD/shared:/shared:Z --shm-size=4G quay.io/drjones/cvm-aarch64
```

Wait a few minutes for the TCG VM to boot...

Then,

```
$ ssh -p 2222 guest@localhost
```

(password is 'password')

## [optional] Make it easier to connect

```
$ cat <<EOF >>.ssh/config

Host autosig
  Hostname localhost
  User guest
  Port 2222
EOF
$ ssh-copy-id autosig  # password is 'password'
$ ssh autosig
```

(The first login takes several seconds, but later logins are faster.)

# After ssh'ing, test the shared directory

```
[guest@localhost ~]$ echo blah blah > /shared/some-file
[guest@localhost ~]$ exit
$ cat shared/some-file
blah blah
```
